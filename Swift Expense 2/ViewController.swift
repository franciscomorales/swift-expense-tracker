//
//  ViewController.swift
//  Swift Expense 2
//
//  Created by Francisco Morales on 10/6/17.
//  Copyright © 2017 Francisco Morales. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var corePaymentList: [PaymentData] = []
    var selectedRow: PaymentData?

    @IBOutlet var tableView: UITableView!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (corePaymentList.count)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = corePaymentList[indexPath.row].nameOfPlace! + " - $" + String(describing: corePaymentList[indexPath.row].amount)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        selectedRow = corePaymentList[indexPath.row]
        self.performSegue(withIdentifier: "addNew", sender: self)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let task = corePaymentList[indexPath.row]
            context.delete(task)
            (UIApplication.shared.delegate as! AppDelegate).saveContext()
            
            do {
                corePaymentList = try context.fetch(PaymentData.fetchRequest())
            } catch {
                print("Fetching Failed")
            }
        }
        tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        getData()
        self.tableView.reloadData()
        print(corePaymentList)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getData()
        
        tableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //COMPLETE SEGUE
        if segue.identifier == "addNew" {
            let nextVC = segue.destination as! SecondViewController
            nextVC.receivedData = selectedRow
        }
    }
    
    func getData() {
        do {
            corePaymentList = try context.fetch(PaymentData.fetchRequest())
        }
        catch {
            print("Fetching Failed")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}


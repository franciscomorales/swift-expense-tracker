//
//  SecondViewController.swift
//  Swift Expense 2
//
//  Created by Francisco Morales on 10/6/17.
//  Copyright © 2017 Francisco Morales. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController, UITextFieldDelegate {

    //UIOutlets
    @IBOutlet var nameTxtField: UITextField!
    @IBOutlet var amountTxtField: UITextField!
    @IBOutlet var paymentTxtField: UITextField!
    @IBOutlet var categoryTxtField: UITextField!
    @IBOutlet var datePicker: UIDatePicker!
    
    let context = (UIApplication.shared.delegate as!AppDelegate).persistentContainer.viewContext
    
    var corePaymentList: [PaymentData] = []
    var receivedData: PaymentData?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Part of the keyboard hiding code
        self.nameTxtField.delegate = self
        self.amountTxtField.delegate = self
        self.paymentTxtField.delegate = self
        self.categoryTxtField.delegate = self
        
        if receivedData != nil
        {
            let amountString = String(format: "%.2f", receivedData!.amount)
            nameTxtField.text = receivedData?.nameOfPlace
            //amountTxtField.text = String(describing: receivedData!.amount)
            amountTxtField.text = amountString
            paymentTxtField.text = receivedData?.formOfPayment
            categoryTxtField.text = receivedData?.category
            datePicker.date = (receivedData?.date)!
        }
        else
        {
            nameTxtField.text = ""
            amountTxtField.text = ""
            paymentTxtField.text = ""
            categoryTxtField.text = ""
        }
        
        // Do any additional setup after loading the view.
        let saveBtn = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.save, target: self, action: #selector(buttonTapped(_:)))
        
        //IMPLEMENTED SWIPE LEFT TO DELETE
        let deleteBtn = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.trash, target: self, action: #selector(trashTapped(_:)))
        
        self.navigationItem.rightBarButtonItems = [saveBtn]
    }
    
    @objc func buttonTapped (_ sender : UIButton) {
        print("saved")
        let data = PaymentData(context: context)
        
        data.nameOfPlace = nameTxtField.text
        data.amount = (amountTxtField.text! as NSString).doubleValue
        data.formOfPayment = paymentTxtField.text
        data.category = categoryTxtField.text
        data.date = datePicker.date
        
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
        
        let alertController = UIAlertController(title: "Expense Saved!", message:
            "", preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
        
        nameTxtField.text = ""
        amountTxtField.text = ""
        paymentTxtField.text = ""
        categoryTxtField.text = ""
    }
    
    @objc func trashTapped (_ sender : UIButton) {
        print("trash")
        //IMPLEMENTED SWIPE LEFT TO DELETE
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //HIDE KEYBOARD
    //Presses outside of keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    //Presses return key
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        nameTxtField.resignFirstResponder()
        amountTxtField.resignFirstResponder()
        paymentTxtField.resignFirstResponder()
        categoryTxtField.resignFirstResponder()
        return (true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
